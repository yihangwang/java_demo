package tk.wangyihang.restfulapi.resource.impl;

import tk.wangyihang.restfulapi.resource.Resource;
import tk.wangyihang.restfulapi.service.IService;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tk.wangyihang.restfulapi.domain.Movie;

@RestController
@RequestMapping("/movies")
@CrossOrigin(origins="http://localhost:3000/")
public class MovieResourceImpl implements Resource<Movie> {
	@Autowired
	private IService<Movie> movieService;
	
	@Override
	public ResponseEntity<Collection<Movie>> findAll() {
		return new ResponseEntity(movieService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Movie> findById(long id) {
		return new ResponseEntity(movieService.findById(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Movie> save(Movie movie) {
		return new ResponseEntity(movieService.saveOrUpdate(movie), HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<Movie> update(Movie movie) {
		return new ResponseEntity(movieService.saveOrUpdate(movie), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> deleteById(long id) {
		return new ResponseEntity(movieService.deleteById(id), HttpStatus.OK);
	}

}
