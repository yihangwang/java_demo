package tk.wangyihang.restfulapi.service.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tk.wangyihang.restfulapi.domain.Movie;
import tk.wangyihang.restfulapi.repository.MovieRepository;
import tk.wangyihang.restfulapi.service.IService;

@Service
public class MovieServiceImpl implements IService<Movie> {

	@Autowired
	private MovieRepository movieRepository;
	
	@Override
	public Collection<Movie> findAll() {
		return movieRepository.findAll();
	}

	@Override
	public Movie findById(long id) {
		return movieRepository.findById(id).get();
	}

	@Override
	public Movie saveOrUpdate(Movie movie) {
		return movieRepository.saveAndFlush(movie);
	}

	@Override
	public String deleteById(long id) {
		JSONObject jsonObject = new JSONObject();
	   
	    try {
	    	movieRepository.deleteById(id);
			jsonObject.put("message", "Movie deleted successfully");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

}
