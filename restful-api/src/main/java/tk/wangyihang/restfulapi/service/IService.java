package tk.wangyihang.restfulapi.service;

import java.util.Collection;
import tk.wangyihang.restfulapi.domain.Movie;


public interface IService<T> {
	Collection<T> findAll();
	
	T findById(long id);
	
	T saveOrUpdate(T t);
		
	String deleteById(long id);
}