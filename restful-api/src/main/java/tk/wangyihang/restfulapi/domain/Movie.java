package tk.wangyihang.restfulapi.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Movie {
	@Id
	@GeneratedValue
	private long id;
	private String title;
	private int year;
	private String coverPhotoURL;
	
	public long getId() 
	{
		return id;
	}
	
	public void setId(long id) 
	{
		this.id = id;
	}
	
	public String getTitle() 
	{
		return title;
	}
	
	public void setTitle(String title) 
	{
		this.title = title;
	}
	
	public int getYear() 
	{
		return year;
	}
	
	public void setYear(int year) 
	{
		this.year = year;
	}
	
	public String getCoverPhotoURL() 
	{
		return coverPhotoURL;
	}
	
	public void setCoverPhotoURL(String coverPhotoURL) 
	{
		this.coverPhotoURL = coverPhotoURL;
	}
}
