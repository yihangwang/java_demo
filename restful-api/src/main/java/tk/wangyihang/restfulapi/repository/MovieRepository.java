package tk.wangyihang.restfulapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tk.wangyihang.restfulapi.domain.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>{

}
