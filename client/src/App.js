import './App.css';
import { Container, Row, Col } from "react-bootstrap";
import { BrowserRouter, Switch, Route } from "react-router-dom";

// Import Components
import NavigationBar from "./components/NavigationBar";
import Welcome from "./components/Welcome";
import Footer from "./components/Footer";
import MovieList from "./components/MovieList";
import Movie from "./components/Movie";
import UserList from "./components/UserList";
import Login from "./components/Login";

function App() {
  const marginTop = {
  	marginTop:'20px'
  }

  return (
    <BrowserRouter>
	  <NavigationBar />
	  
	  <Container>
      	<Row>
			<Col lg={12} className={ marginTop }>
			
				<Switch>
					<Route path="/" exact component={Welcome} />
					<Route path="/list" exact component={MovieList} />
					<Route path="/add" exact component={Movie} />
					<Route path="/edit/:id" exact component={Movie} />
					<Route path="/users" exact component={UserList} />
					<Route path="/login" exact component={Login} />
				</Switch>
				
			</Col>
		</Row>
	  </Container>

	  <Footer />
    </BrowserRouter>
  );
}

export default App;
