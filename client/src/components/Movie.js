import { React, Component } from 'react';
import { Card, Form, Button, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusSquare, faSave, faUndo, faList, faEdit } from "@fortawesome/free-solid-svg-icons";

// Import Components
import MyToast from "./MyToast";

class Movie extends Component
{
	constructor(props)
	{
		super(props);
		this.state = this.initialState;
		this.state.show = false;
	}
	
	initialState = {
			id: '',
			title:'',
			year:'',
			coverPhotoURL:''
	};
	
	componentDidMount() {
	    const movieId = +this.props.match.params.id;
		
		this.findMovieById(movieId);
	}
	
	findMovieById = (movieId) => {
		fetch("http://localhost:8080/rest/movies/" + movieId)
			.then(response => response.json())
			.then((movie) => {
				if(movie){
					this.setState({
						id: movie.id,
						title:movie.title,
						year:movie.year,
						coverPhotoURL:movie.coverPhotoURL
					});
				}
			})
			.catch((error) => console.error("Error - " + error ));
	}
	
	resetMovie = () =>
	{
		this.setState(() => this.initialState);
	}

	submitMovie = (event) =>
	{
		event.preventDefault();

		const movie = {
			title: this.state.title,
			year: this.state.year,
			coverPhotoURL: this.state.coverPhotoURL
		};
		
		const headers = new Headers();
		headers.append('Content-Type', 'application/json')
		
		fetch("http://localhost:8080/rest/movies", {
			method: "POST",
			body: JSON.stringify(movie),
			headers
		}).then(response => response.json())
			.then(movie => {
				if(movie)
				{
					this.setState({"show": true, "method": "post"});
					setTimeout(() => this.setState({"show": false}), 3000);
				}
				else
				{
					this.setState({"show": false});
				}
			});
		
		this.setState(this.initialState);
	}
	
	updateMovie = (event) =>
	{
		event.preventDefault();

		const movie = {
			id: this.state.id,
			title: this.state.title,
			year: this.state.year,
			coverPhotoURL: this.state.coverPhotoURL
		};
		
		const headers = new Headers();
		headers.append('Content-Type', 'application/json')
		
		fetch("http://localhost:8080/rest/movies", {
			method: "PUT",
			body: JSON.stringify(movie),
			headers
		})
			.then(response => response.json())
			.then(movie => {
				if(movie)
				{
					this.setState({"show": true, "method": "put"});
					setTimeout(() => this.setState({"show": false}), 3000);
					setTimeout(() => this.movieList(), 3000);
				}
				else
				{
					this.setState({"show": false});
				}
			});
		
		this.setState(this.initialState);
	}
	
	movieChange = (event) =>
	{
		this.setState({
			[event.target.name]:event.target.value
		});
	}
	
	movieList = () => {
		return this.props.history.push("/list");
	};
	
	render()
	{
		const {title, year, coverPhotoURL} = this.state;
	
		return (
			<div>
				<div style={{"display": this.state.show ? "block" : "none"}}>
					<MyToast show={this.state.show} message={this.state.method === "put" ? "Movie Updated Successfully" : "Movie Saved Successfully"} type={"success"} />
				</div>
				
				<Form id="movieFormId" onSubmit={this.state.id ? this.updateMovie : this.submitMovie} onReset={this.resetMovie}>
					<Card className={"border border-dark bg-dark text-white"}>
						<Card.Header><FontAwesomeIcon icon= {this.state.id ? faEdit : faPlusSquare} />  {this.state.id ? "Update Movie" : "Add Movie"}</Card.Header>
						<Card.Body>
							  <Row>
								  <Form.Group as={Col} controlId="formGridTitle">
								    <Form.Label>Title</Form.Label>
								    <Form.Control 
										type="text" 
										name="title" 
										value={title} require="true" autoComplete="off"
										onChange={this.movieChange}
										placeholder="Enter Movie Title" 
										className={"bg-dark text-white"} />
								  </Form.Group>
									
								  <Form.Group as={Col} controlId="formGridYear">
								    <Form.Label>Year</Form.Label>
								    <Form.Control 
										type="text" 
										name="year" 
										value={year} require="true" autoComplete="off"
										onChange={this.movieChange}
										placeholder="Enter Movie Year" 
										className={"bg-dark text-white"} />
								  </Form.Group>
							  </Row>
							  
							  <Row>
								  <Form.Group as={Col} controlId="formGridCoverPhotoURL">
								    <Form.Label>Cover Photo URL</Form.Label>
								    <Form.Control 
										type="text" 
										name="coverPhotoURL" 
										value={coverPhotoURL} require="true" autoComplete="off"
										onChange={this.movieChange}
										placeholder="Enter Movie Cover Photo URL" 
										className={"bg-dark text-white"} />
								  </Form.Group>
							  </Row>
							
						</Card.Body>
						<Card.Footer style={{"textAlign":"right"}}>
							<Button size="sm" variant="success" type="submit">
								<FontAwesomeIcon icon={faSave} /> {this.state.id ? "Update" : "Submit"}
							</Button>{ ' ' }
							<Button size="sm" variant="info" type="reset">
								<FontAwesomeIcon icon={faUndo} /> Reset
							</Button>{ ' ' }
							<Button size="sm" variant="info" type="button" onClick={this.movieList.bind()}>
								<FontAwesomeIcon icon={faList} /> Movie List
							</Button>
						</Card.Footer>
					</Card>
				</Form>
			</div>
			
		);
	}
}

export default Movie;