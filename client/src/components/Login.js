import { React, Component } from 'react';
import { Row, Col, Card, Form, InputGroup, FormControl, Button} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignInAlt, faEnvelope, faLock, faUndo } from "@fortawesome/free-solid-svg-icons";

class Login extends Component
{
	constructor(props)
	{
		super(props);
		this.state = this.initialState;
	}
	
	initialState = {
			email: '',
			password:''
	};
	
	resetLoginForm = () =>
	{
		this.setState(() => this.initialState);
	}
	
	credentialChange = (event) =>
	{
		this.setState({
			[event.target.name]:event.target.value
		});
	}

	render()
	{
		const {email, password} = this.state;
	
		return (
			<Row className="justify-content-md-center">
				<Col xs={5}>
					<Card className={"border border-dark bg-dark text-white"}>
						<Card.Header>
							<FontAwesomeIcon icon={faSignInAlt} /> Login
						</Card.Header>
						<Card.Body>
							<Row>
								<Form.Group as={Col}>
									<InputGroup>
										<InputGroup.Text>
											<FontAwesomeIcon icon={faEnvelope} />
										</InputGroup.Text>
										<FormControl required="true" autoComplete="off" type="text" name="email"
											value={email} className={"bg-dark text-white"} placeholder="Enter Email Address" 
											onChange={this.credentialChange}/>
									</InputGroup>
								</Form.Group>
							</Row>
							<br/>
							<Row>
								<Form.Group as={Col}>
									<InputGroup>
										<InputGroup.Text>
											<FontAwesomeIcon icon={faLock} />
										</InputGroup.Text>
										<FormControl required="true" autoComplete="off" type="password" name="password"
											value={password} className={"bg-dark text-white"} placeholder="Enter Password" 
											onChange={this.credentialChange} />
									</InputGroup>
								</Form.Group>
							</Row>
						</Card.Body>
						<Card.Footer style={{"text-align":"right"}}>
							<Button size="sm" type="button" variant="success"
								disabled={this.state.email.lenght === 0 || this.state.password.lenght === 0 }
								onClick={this.resetLoginForm}>
								<FontAwesomeIcon icon={faSignInAlt} /> Login
							</Button>{ " " }
							<Button size="sm" type="button" variant="info"
								disabled={this.state.email.lenght === 0 && this.state.password.lenght === 0 }
								onClick={this.resetLoginForm}>
								<FontAwesomeIcon icon={faUndo} /> Reset
							</Button>
						</Card.Footer>
					</Card>
				</Col>
			</Row>
		);
	}
}

export default Login;