import { React, Component } from 'react';
import { Card, Table, Image, ButtonGroup, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList, faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

// Import Components
import MyToast from "./MyToast";

class MovieList extends Component
{
	constructor(props) {
	    super(props);
	    this.state = {
	      movies: []
	    };
	}
	
	componentDidMount() {
	    this.findAllMovies();
	}
	
	findAllMovies() {
		fetch("http://localhost:8080/rest/movies")
			.then(response => response.json())
			.then((data) => {
				this.setState({movies: data});
			});
	}
	
	deleteMovie = (movieId) => {
		fetch("http://localhost:8080/rest/movies/" + movieId, {
			method: 'DELETE'
		})
			.then(response => response.json())
			.then(movie => {
				if(movie){
					this.setState({
						movies: this.state.movies.filter(movie => movie.id !== movieId)
					});
					this.setState({"show": true});
					setTimeout(() => this.setState({"show": false}), 3000);
				}
				else
				{
					this.setState({"show": false});
				}
			});
	}
	
	render()
	{
		return (
			<div>
				<div style={{"display": this.state.show ? "block" : "none"}}>
					<MyToast show={this.state.show} message={"Movie Deleted Successfully"} type={"danger"} />
				</div>
				
				<Card className={"border border-dark bg-dark text-white"}>
					<Card.Header>
						<FontAwesomeIcon icon={faList} /> Movie List
					</Card.Header>
					<Card.Body>
						<Table striped bordered hover variant="dark">
						  <thead>
						    <tr>
						      <th>#</th>
						      <th>Title</th>
						      <th>Year</th>
						      <th>Cover Photo</th>
							  <th>Action</th>
						    </tr>
						  </thead>
						  <tbody>
							{
								this.state.movies.length === 0
									? (<tr align="center">
											<td colSpan="5"> No Movie Available</td>
									    </tr>)
									: (this.state.movies.map((movie) => (
										<tr>
											<td>{movie.id}</td>
											<td>{movie.title}</td>
											<td>{movie.year}</td>
											<td><Image src={movie.coverPhotoURL} height="50" /></td>
											<td>
												<ButtonGroup>
													<Link to={"/edit/" + movie.id} className="btn btn-sm btn-outline-primary"><FontAwesomeIcon icon={faEdit} /></Link>{ " " }
													<Button size="sm" variant="outline-danger" onClick={this.deleteMovie.bind(this, movie.id)}><FontAwesomeIcon icon={faTrash} /></Button>
												</ButtonGroup>
											</td>
										</tr>
									)))
							}
						  </tbody>
						</Table>
					</Card.Body>
	
				</Card>
			</div>
		);
	}
}

export default MovieList;