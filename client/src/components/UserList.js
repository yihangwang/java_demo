import { React, Component } from 'react';
import { Card, Table, InputGroup, FormControl, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUsers, faStepBackward, faFastBackward, faStepForward, faFastForward } from "@fortawesome/free-solid-svg-icons";

class UserList extends Component
{
	constructor(props) {
	    super(props);
	    this.state = {
	      users: [],
		  currentPage: 1,
		  usersPerPage: 5
	    };
	}
	
	componentDidMount() {
	    this.findAllRandomUser();
	}
	
	findAllRandomUser() {
		fetch("https://randomapi.com/api/6de6abfedb24f889e0b5f675edc50deb?fmt=raw&sole")
			.then(response => response.json())
			.then((data) => {
				this.setState({users: data});
			});
	}
	
	changePage = (event) =>
	{
		this.setState({
			[event.target.name]: parseInt(event.target.value)
		});
	}
	
	firstPage = () => {
	   if (this.state.currentPage > 1) {
	      this.setState({
	        currentPage: 1,
	      });
	   }
	};
	
	prevPage = () => {
	   if (this.state.currentPage > 1) {
	      this.setState({
	        currentPage: this.state.currentPage - 1,
	      });
	   }
	};
	
	nextPage = () => {
	   if (this.state.currentPage < Math.ceil(this.state.users.length / this.state.usersPerPage)) {
	      this.setState({
	        currentPage: this.state.currentPage + 1,
	      });
	   }
 	};

	lastPage = () => {
	   if (this.state.currentPage < Math.ceil(this.state.users.length / this.state.usersPerPage)) {
	      this.setState({
	        currentPage: Math.ceil(this.state.users.length / this.state.usersPerPage),
	      });
	   }
	};


	render()
	{
		const {users, currentPage, usersPerPage} = this.state;
		const lastIndex = currentPage * usersPerPage;
		const firstIndex = lastIndex - usersPerPage;
		const currentUsers = users.slice(firstIndex, lastIndex);
		const totalPages = users.length / usersPerPage;
		
		const pageNumCss = {
			width: "45px",
			border: "1px solid #17A2B8",
			color: "#17A2B8",
			texAlign: "center",
			fontWeight: "bold"
		}

		return (
			<Card className={"border border-dark bg-dark text-white"}>
					<Card.Header>
						<FontAwesomeIcon icon={faUsers} /> User List
					</Card.Header>
					<Card.Body>
						<Table striped bordered hover variant="dark">
						  <thead>
						    <tr>
						      <th>Name</th>
						      <th>Email</th>
						      <th>Address</th>
						      <th>Created</th>
						    </tr>
						  </thead>
						  <tbody>
							{
								users.length === 0
									? (<tr align="center">
											<td colSpan="4"> No User Available</td>
									    </tr>)
									: (currentUsers.map((user, index) => (
										<tr key={index}>
											<td>{user.first}{" "}{user.last}</td>
											<td>{user.email}</td>
											<td>{user.address}</td>
											<td>{user.created}</td>
										</tr>
									)))
							}
						  </tbody>
						</Table>
					</Card.Body>
					<Card.Footer>
						<div style={{"float":"left"}}>
							Showing Page {currentPage} of {totalPages}
						</div>
						<div style={{"float":"right"}}>
							<InputGroup >
								<Button type="button" variant="outline-info" disabled={currentPage === 1 ? true : false}
									onClick={this.firstPage}>
									<FontAwesomeIcon icon={faFastBackward} /> First
								</Button>
								<Button type="button" variant="outline-info" disabled={currentPage === 1 ? true : false}
									onClick={this.prevPage}>
									<FontAwesomeIcon icon={faStepBackward} /> Prev
								</Button>
								<FormControl className={"bg-dark"} style={pageNumCss} name="currentPage" value={currentPage}
									onChange={this.changePage}/>
								<Button type="button" variant="outline-info" disabled={currentPage === totalPages ? true : false}
									onClick={this.nextPage}>
									<FontAwesomeIcon icon={faStepForward} /> Next
								</Button>
								<Button type="button" variant="outline-info" disabled={currentPage === totalPages ? true : false}
									onClick={this.lastPage}>
									<FontAwesomeIcon icon={faFastForward} /> Last
								</Button>
							</InputGroup>
						</div>
					</Card.Footer>
				</Card>
		);
	}
}

export default UserList;