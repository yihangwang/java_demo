import { React, Component } from 'react';
import { Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignInAlt } from "@fortawesome/free-solid-svg-icons";

class NavigationBar extends Component
{
	render()
	{
		return (
			<Navbar bg="dark" variant="dark">
			
				<Link to={""} className="navbar-brand">
					Java Demo
				</Link>
			
				<Nav className="me-auto">
					<Link to="list" className="nav-link">Movie List</Link>
				    <Link to="add" className="nav-link">Add Movie</Link>
					<Link to="users" className="nav-link">User List</Link>
				</Nav>
				
				<Nav className="ml-auto">
					<Link to="login" className="nav-link"><FontAwesomeIcon icon={faSignInAlt} /> Login</Link>
				</Nav>
			</Navbar>
		);
	}
}

export default NavigationBar;